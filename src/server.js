// Require some Node.js Frameworks
const express = require('express');

// Creating an express web api and pass the port number as an argument
const server = express();
const PORT = process.argv[2];

// Define the root path on the web api
server.get('/js/nash.js', (req, res) => {
    res.sendfile(__dirname + '/public/js/nash.js');
});

server.get('/', (req, res) => {
    res.sendfile(__dirname + '/public/index.html');
});

// Running the api on the port (argument)
server.listen(PORT, () => {
    console.log(`Server is now running on port ${PORT}`);
});
