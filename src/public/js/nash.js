var width = 800,
    height = 500,
    radius = 20;

var field_size = 6;
let colors = ['white', 'red', 'blue'];

let next = 1;

var svg = d3
    .select('#chart')
    .append('svg')
    .attr('width', width)
    .attr('height', height);

var data = getHexFields(field_size);
var scaleX = d3
    .scaleLinear()
    .range([10, height - 10 * 2])
    .domain([0, 1.8 * field_size]);
var scaleY = d3
    .scaleLinear()
    .range([10, height - 10 * 2])
    .domain([1.8 * field_size, 0]);

let borderData = getBorderData(field_size);
svg
    .selectAll('.border')
    .data(borderData)
    .enter()
    .append('polygon')
    .attr('class', 'border')
    .attr('points', d => {
        let p = d.points;
        return p
            .map(function(p) {
                return [scaleX(p.x), scaleY(p.y)].join(',');
            })
            .join(' ');
    })
    .attr('stroke', 'black')
    .attr('stroke-width', 2)
    .attr('fill', d => {
        return colors[d.fill];
    });

svg
    .selectAll('.hex')
    .data(data)
    .enter()
    .append('polygon')
    .attr('class', 'hex')
    .attr('points', d => {
        let p = d.points;
        return p
            .map(function(p) {
                return [scaleX(p.x), scaleY(p.y)].join(',');
            })
            .join(' ');
    })
    .attr('stroke', 'black')
    .attr('stroke-width', 2)
    .attr('fill', d => {
        return colors[d.fill];
    })
    .on(
        'click',
        (function(d) {
            return function(d) {
                if (d.fill == 0) {
                    d.fill = next;
                    d3.select(this).style('fill', colors[d.fill]);
                    if (next == 1) {
                        next = 2;
                    } else {
                        next = 1;
                    }
                }
            };
        })()
    );

function getHexFields(size) {
    var hexFields = [];

    var h = Math.sqrt(3) / 2;

    var xp_start = size + h;
    for (var yi = 0; yi < size; yi++) {
        xp_start -= h;
        for (var xi = 0; xi < size; xi++) {
            let xp = xp_start + 2 * h * xi;
            let yp = yi * 1.5 + 0.5;
            let poly = {
                points: [
                    { x: xp, y: yp },
                    { x: xp + h, y: yp + 0.5 },
                    { x: xp + h, y: yp + 1.5 },
                    { x: xp, y: yp + 2 },
                    { x: xp - h, y: yp + 1.5 },
                    { x: xp - h, y: yp + 0.5 },
                ],
                fill: 0,
            };

            hexFields.push(poly);
        }
    }
    return hexFields;
}

function getBorderData(size) {
    var hexFields = [];

    var h = Math.sqrt(3) / 2;
    var xp_start = size;
    let yp = 0.5;
    let xp;

    // bottom
    var points = [];
    for (var xi = 0; xi < size; xi++) {
        xp = xp_start + 2 * h * xi;
        points.push({ x: xp - h, y: yp + 0.5 });
        points.push({ x: xp, y: yp });
        points.push({ x: xp + h, y: yp + 0.5 });
    }
    points.push({ x: xp + h, y: 0 });
    points.push({ x: xp_start - h, y: 0 });
    let poly = {
        points: points,
        fill: 1,
    };
    hexFields.push(poly);

    // top
    xp_start = size - (size - 1) * h;
    var points = [];
    yp = (size - 1) * 1.5 + 0.5;
    for (var xi = 0; xi < size; xi++) {
        xp = xp_start + 2 * h * xi;
        points.push({ x: xp - h, y: yp + 1.5 });
        points.push({ x: xp, y: yp + 2 });
        points.push({ x: xp + h, y: yp + 1.5 });
    }
    points.push({ x: xp + h, y: yp + 2.5 });
    points.push({ x: xp_start - h, y: yp + 2.5 });
    poly = {
        points: points,
        fill: 1,
    };
    hexFields.push(poly);

    // left
    xp_start = size + h;
    var points = [];
    xp = xp_start;
    for (var yi = 0; yi < size; yi++) {
        xp -= h;
        yp = yi * 1.5 + 0.5;
        points.push({ x: xp - h, y: yp + 0.5 });
        points.push({ x: xp - h, y: yp + 1.5 });
    }
    points.push({ x: xp - h - 1, y: yp + 1.5 });
    points.push({ x: size - 1.5, y: 1 });
    poly = {
        points: points,
        fill: 2,
    };
    hexFields.push(poly);

    // right
    xp_start = size + h + 2 * h * (size - 1);
    var points = [];
    xp = xp_start;
    for (var yi = 0; yi < size; yi++) {
        xp -= h;
        yp = yi * 1.5 + 0.5;
        points.push({ x: xp + h, y: yp + 0.5 });
        points.push({ x: xp + h, y: yp + 1.5 });
    }
    points.push({ x: xp + h + 1, y: yp + 1.5 });
    points.push({ x: xp_start + 1, y: 1 });
    poly = {
        points: points,
        fill: 2,
    };
    hexFields.push(poly);

    return hexFields;
}
